import axios from "axios";


const apiClient = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com/posts",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  }
});


export default {

  getPosts() {
    return apiClient.get("/");
  },

  getPost(id: any) {
    return apiClient.get("/" + id);
  },

  deletePost(id: any) {
    return new Promise((resolve, reject) => {
      apiClient.delete("/" + id).then(response => {
        console.log("data deleted" + response);
        resolve(response);
      })
        .catch(function (error) {
          console.log(error.response)
        });
    });
  }
}
